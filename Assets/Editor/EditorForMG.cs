﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapGenerator))]
[CanEditMultipleObjects]
public class EditorForMG : Editor
{
    public MapGenerator mpScript = null;
    SerializedProperty _width, _height, _generateMapRandomly, _black, _white, _player, _navMesh;
    
    public void OnEnable()
    {
        mpScript = (MapGenerator)target;

        _width = serializedObject.FindProperty("width");
        _height = serializedObject.FindProperty("height");

        _black = serializedObject.FindProperty("black");
        _white = serializedObject.FindProperty("white");
        _player= serializedObject.FindProperty("player");
        _navMesh = serializedObject.FindProperty("m_navMesh");

        _generateMapRandomly = serializedObject.FindProperty("generateMapRandomly");
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        
        // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
        serializedObject.Update();

        EditorGUILayout.PropertyField(_generateMapRandomly, new GUIContent("Generate Map Randomly"));
        mpScript.generateMapRandomly = _generateMapRandomly.boolValue;

        EditorGUILayout.PropertyField(_black, new GUIContent("Black"));
        mpScript.black = _black.objectReferenceValue as GameObject;

        EditorGUILayout.PropertyField(_white, new GUIContent("White"));
        mpScript.white = _white.objectReferenceValue as GameObject;

        EditorGUILayout.PropertyField(_player, new GUIContent("Player"));
        mpScript.player = _player.objectReferenceValue as GameObject;

        EditorGUILayout.PropertyField(_navMesh, new GUIContent("NavMesh"));
        mpScript.m_navMesh = _navMesh.objectReferenceValue as RAIN.Navigation.NavMesh.NavMeshRig;

        if (mpScript.generateMapRandomly == false)
        {
            EditorGUILayout.PropertyField(_width, new GUIContent("Width"));
            mpScript.width = _width.intValue;

            EditorGUILayout.PropertyField(_height, new GUIContent("Height"));
            mpScript.height = _height.intValue;
        }
    }
}
