﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.Runtime.InteropServices;

[CustomEditor(typeof(Pooler))]
[CanEditMultipleObjects]
public class EditorForObjectPooling : Editor
{
    public Pooler opScript = null;

    SerializedProperty _total_Amount_Of_Objects, _objects_To_Instantiate, _amount_Of_Objects_To_Instantiate;

    public void OnEnable()
    {
        opScript = (Pooler)target;
    }

    public override void OnInspectorGUI()
    {
        _total_Amount_Of_Objects = serializedObject.FindProperty("m_total_Amount_Of_Objects");

        GUILayout.BeginHorizontal("box");

        EditorGUILayout.PropertyField(_total_Amount_Of_Objects, new GUIContent(""));

        GUILayout.EndHorizontal();

        opScript.m_total_Amount_Of_Objects = _total_Amount_Of_Objects.intValue;
        
        GUILayout.BeginHorizontal("box");

        if (GUILayout.Button("+"))
        {
            _total_Amount_Of_Objects.intValue++;
        }

        if (GUILayout.Button("-"))
        {
            if (_total_Amount_Of_Objects.intValue == 0)
                return;

            _total_Amount_Of_Objects.intValue--;
        }

        GUILayout.EndHorizontal();

        _objects_To_Instantiate = serializedObject.FindProperty("m_objects_To_Instantiate");

        _amount_Of_Objects_To_Instantiate = serializedObject.FindProperty("m_amount_Of_Objects_To_Instantiate");

        EditorGUILayout.BeginVertical();

        _objects_To_Instantiate.arraySize = _total_Amount_Of_Objects.intValue;

        _amount_Of_Objects_To_Instantiate.arraySize = _total_Amount_Of_Objects.intValue;

        opScript.m_total_Amount_Of_Objects = _total_Amount_Of_Objects.intValue;

        for (int i = 0; i <= _total_Amount_Of_Objects.intValue; i++)
        {
            if (i != 0)
            {
                GUILayout.Space(10f);

                EditorGUILayout.BeginVertical("Box");

                CreateTitle(12, TextAnchor.MiddleCenter, FontStyle.Bold, "Object: " + i);

                if (_objects_To_Instantiate.NextVisible(true))
                {
                    EditorGUILayout.PropertyField(_objects_To_Instantiate, new GUIContent(""));
                }

                if (_amount_Of_Objects_To_Instantiate.NextVisible(true))
                {
                    EditorGUILayout.PropertyField(_amount_Of_Objects_To_Instantiate, new GUIContent(""));
                }

                EditorGUILayout.EndVertical();
            }
            else
            {
                _objects_To_Instantiate.NextVisible(true);
                _amount_Of_Objects_To_Instantiate.NextVisible(true);
            }
        }

        EditorGUILayout.EndVertical();
    }

    public void CreateTitle(int a_fontSize, TextAnchor a_alignment, FontStyle a_style, string a_titleText, [Optional] Color a_color)
    {
        GUIStyle guiStyle = new GUIStyle();

        guiStyle.fontSize = a_fontSize;
        guiStyle.alignment = a_alignment;
        guiStyle.fontStyle = a_style;

        if (a_color == null)
            guiStyle.normal.textColor = a_color;
        else
            guiStyle.normal.textColor = Color.black;

        GUILayout.Label(a_titleText, guiStyle);

        GUILayout.Space(10f);
    }
}