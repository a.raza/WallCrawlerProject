﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CameraFollow))]
[CanEditMultipleObjects]
public class EditorForCameraFollow : Editor
{
    public CameraFollow cfScript = null;

    SerializedProperty 
    _camSpeed,
    _camFollower,
    _clampAngle,
    _inputSensitivity,
    _cam,
    _player;

    public void OnEnable()
    {
        cfScript = (CameraFollow)target;

        _camSpeed = serializedObject.FindProperty("m_camSpeed");
        _clampAngle = serializedObject.FindProperty("m_clampAngle");
        _inputSensitivity = serializedObject.FindProperty("m_inputSensitivity");
        _camFollower = serializedObject.FindProperty("m_camFollower");
        _cam = serializedObject.FindProperty("m_cam");
        _player = serializedObject.FindProperty("m_player");
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.BeginVertical("Box");

        CreateTitle(12, TextAnchor.MiddleCenter, FontStyle.Bold, "Camera Moving Speed");
        CreateVariable(_camSpeed, cfScript.m_camSpeed, _camSpeed.floatValue);

        CreateTitle(12, TextAnchor.MiddleCenter, FontStyle.Bold, "Object That Follow Camera");
        CreateVariable(_clampAngle, cfScript.m_clampAngle, _clampAngle.floatValue);

        CreateTitle(12, TextAnchor.MiddleCenter, FontStyle.Bold, "Range Of The Cam");
        CreateVariable(_inputSensitivity, cfScript.m_inputSensitivity, _inputSensitivity.floatValue);

        CreateTitle(12, TextAnchor.MiddleCenter, FontStyle.Bold, "Sensitivity Of Mouse Movement");
        CreateVariable(_camFollower, cfScript.m_camFollower, _camFollower.objectReferenceValue as GameObject);

        CreateTitle(12, TextAnchor.MiddleCenter, FontStyle.Bold, "Main Camera");
        CreateVariable(_cam, cfScript.m_cam, _cam.objectReferenceValue as GameObject);

        CreateTitle(12, TextAnchor.MiddleCenter, FontStyle.Bold, "Object That Will Be Focused On");
        CreateVariable(_player, cfScript.m_player, _player.objectReferenceValue as GameObject);

        EditorGUILayout.EndVertical();
    }

    protected void CreateVariable(SerializedProperty a_variable, float a_setVariable, float a_getVariable)
    {
        EditorGUILayout.PropertyField(a_variable, new GUIContent(""));
        a_setVariable = a_getVariable;
    }

    protected void CreateVariable(SerializedProperty a_variable, GameObject a_setVariable, GameObject a_getVariable)
    {
        EditorGUILayout.PropertyField(a_variable, new GUIContent(""));
        a_setVariable = a_getVariable;
    }

    public void CreateTitle(int a_fontSize, TextAnchor a_alignment, FontStyle a_style, string a_titleText)
    {
        GUILayout.Space(10f);
        GUIStyle guiStyle = new GUIStyle();

        guiStyle.fontSize = a_fontSize;
        guiStyle.alignment = a_alignment;
        guiStyle.fontStyle = a_style;

        GUILayout.Label(a_titleText, guiStyle);
    }
}
