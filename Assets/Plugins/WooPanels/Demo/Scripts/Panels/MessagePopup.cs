using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Wooplex.Panels;

public class MessagePopup : WooPanel
{
	public Text MessageText;

	// At the beginning even if the panel is closed 
	void OnInit ()
	{
		
	}

	public void ShowMessage(string message)
	{
		MessageText.text = message;
		Open();
	}

	// Before animation starts
	void OnOpen ()
	{
		
	}

	// After animation finishes
	void OnOpenEnd ()
	{
		
	}

	// Before animation starts
	void OnClose ()
	{
		
	}

	// After animation finishes
	void OnCloseEnd ()
	{
		
	}

	// Every frame when the panel is opened or being opened
	void PanelUpdate ()
	{
		
	}
}
