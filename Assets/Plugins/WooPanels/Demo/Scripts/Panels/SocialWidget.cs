using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Wooplex.Panels;

public class SocialWidget : WooPanel
{
	public Text TimeText;

	// At the beginning even if the panel is closed 
	void OnInit ()
	{
		PanelManager.IsOpen<WooPanel>();
	}

	// Before animation starts
	void OnOpen ()
	{
		TimeText.text = System.DateTime.Now.ToShortDateString();
	}

	// After animation finishes
	void OnOpenEnd ()
	{
		
	}

	// Before animation starts
	void OnClose ()
	{
		
	}

	// After animation finishes
	void OnCloseEnd ()
	{
		
	}

	public void OnYoutubeClick()
	{
		//Application.OpenURL("www.youtube.com");
		PanelManager.Open<MessagePopup>().ShowMessage("Opening Youtube!");
	}

	public void OnUnityClick()
	{
		//Application.OpenURL("www.unity3d.com");
		PanelManager.Open<MessagePopup>().ShowMessage("Opening Unity!");
	}

	public void OnFacebookClick()
	{
		//Application.OpenURL("www.facebook.com");
		PanelManager.Open<MessagePopup>().ShowMessage("Opening Facebook!");
	}

	// Every frame when the panel is opened or being opened
	void PanelUpdate ()
	{
		
	}
}
