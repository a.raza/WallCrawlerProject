﻿using UnityEngine;
using System.Collections;
using RAIN.Entities;
using RAIN.Core;
using RAIN.Navigation.NavMesh;

public class MapGenerator : MonoBehaviour
{
    [Header("Random Generator")]
    public bool generateMapRandomly;
    [Header("Map Measurements")]
    public int width;
    public int height;
    private string seed;
    private int randomFillPercent = 43, w_Cube = 0;
    protected int[,] map;
    [Header("Prefabs")]
    public GameObject black;
    public GameObject white;
    public GameObject player;
    private Vector3 m_playerPosition;
    public RAIN.Navigation.NavMesh.NavMeshRig m_navMesh;

    void Start()
    {
        if(generateMapRandomly == true)
        {
            width = Random.Range(40, 100);
            height = Random.Range(40, 100);
        }

        m_navMesh.transform.position = new Vector3((int)(width / 2), 0, (int)(height / 2));

        GenerateMap();

        StartCoroutine(GenerateNavMesh());
    }

    IEnumerator GenerateNavMesh()
    {
        yield return new WaitForSeconds(5);

        var n = m_navMesh.NavMesh;

        n.UnregisterNavigationGraph();

        n.Size = (width + height) / 1.5f;

        n.StartCreatingContours(m_navMesh, 5);

        while (n.Creating)
        {
            StartCoroutine(CreateTheCounters(n));
            //System.Threading.Thread.Sleep(10);
        }

        m_navMesh.GenerateAllContourVisuals();

        n.RegisterNavigationGraph();
    }

    IEnumerator CreateTheCounters(NavMesh _n)
    {
        yield return new WaitForSeconds(2);
        _n.CreateContours();
    }

    //private void ReBuildNavMesh()
    //{
    //    RAIN.Navigation.NavMesh.NavMeshRig mesh = rig.NavMesh;
    //    //clear the existing navmesh
    //    mesh.UnregisterNavigationGraph();
    //    mesh.StartCreatingContours(rig, 1);
    //    while (mesh.Creating)
    //    {
    //        mesh.CreateContours();
    //        System.Threading.Thread.Sleep(10);
    //    }
    //    rig.GenerateAllContourVisuals();
    //    mesh.RegisterNavigationGraph();
    //}

    void GenerateMap()
    {
        map = new int[width, height];

        RandomFillMap();

        for (int i = 0; i < 1; i++)
        {
            SmoothMap();
        }
    }


    void RandomFillMap()
    {     
        seed = "";

        //pick 10 random alphabets
        for (int i = 0; i < 10; i++)
        {
            seed += GetLetter();
        }

        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    map[x, y] = 1;
                }
                else
                {
                    map[x, y] = (pseudoRandom.Next(0, 100) < randomFillPercent) ? 1 : 0;
                }
            }
        }
    }


    void SmoothMap()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int neighbourWallTiles = GetSurroundingWallCount(x, y);

                if (neighbourWallTiles > 4)
                {
                    if (black != null)
                        Instantiate(black, new Vector3(x, 0, y), Quaternion.identity);

                    map[x, y] = 1;
                }
                else if (neighbourWallTiles < 4)
                {
                    if (white != null)
                    {
                        //w_Cube++;

                        //if(w_Cube == 5012 && player != null)
                        //{
                        //    m_playerPosition = new Vector3(x, 4, y);

                        //    Instantiate(player, m_playerPosition, Quaternion.identity);

                        //    var c = Instantiate(white, new Vector3(x, 0, y), Quaternion.identity) as GameObject;

                        //    c.name = "Cube " + w_Cube;
                        //}
                        //else 
                        //{
                            var c = Instantiate(white, new Vector3(x, 0, y), Quaternion.identity) as GameObject;

                        //    c.name = "Cube " + w_Cube;
                        //}
                    }
                    map[x, y] = 0;
                }    
            }
        }
    }

    int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;

        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                if (neighbourX >= 0 && neighbourX < width && neighbourY >= 0 && neighbourY < height)
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        wallCount += map[neighbourX, neighbourY];
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }


    //void OnDrawGizmos()
    //{
    //    Vector3 pos = new Vector3();

    //    if (map != null)
    //    {
    //        for (int x = 0; x < width; x++)
    //        {
    //            for (int y = 0; y < height; y++)
    //            {
    //                //Gizmos.color = (map[x, y] == 1) ? Color.black : Color.white;

    //                pos = new Vector3(-width / 2 + x + .5f, 0, -height / 2 + y + .5f);

    //                if(map[x,y] == 1)
    //                {
    //                    Gizmos.color = Color.black;
    //                    Gizmos.DrawCube(pos, new Vector3(4,2,4));
    //                }
    //                else
    //                {
    //                    Gizmos.color = Color.white;
    //                    Gizmos.DrawCube(pos, Vector3.one);
    //                }
    //            }
    //        }
    //    }
    //}

    private char GetLetter()
    {
        string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int num = Random.Range(0, chars.Length - 1);
        return chars[num];
    }
}