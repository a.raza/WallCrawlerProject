﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pooler : MonoBehaviour 
{
    public static Pooler instance = null;

    [Header("Amount Of Object That Needs To Instantiate")]
    public int m_total_Amount_Of_Objects;

    public GameObject[] m_objects_To_Instantiate;

    public int[] m_amount_Of_Objects_To_Instantiate;

    public Vector3[] m_positions;

    public Transform[] m_transforms;

    public bool m_setPositionsAsVector;

    private GameObject[][] m_set_Of_Pool;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        Manage_SetOfPool();

        Create_SetsOfObjects();
    }

    private void Create_SetsOfObjects()
    {
        for (int i = 0; i < m_objects_To_Instantiate.Length; i++)
        {
            for (int j = 0; j < m_amount_Of_Objects_To_Instantiate[i]; j++)
            {
                if(m_setPositionsAsVector.Equals(true))
                    m_set_Of_Pool[i][j] = Instantiate(m_objects_To_Instantiate[i], m_positions[i], Quaternion.identity) as GameObject;
                else
                    m_set_Of_Pool[i][j] = Instantiate(m_objects_To_Instantiate[i], m_transforms[i].position, Quaternion.identity) as GameObject;
                
                m_set_Of_Pool[i][j].SetActive(false);
            }
        }
    }

    private void Manage_SetOfPool()
    {
        m_set_Of_Pool = new GameObject[m_objects_To_Instantiate.Length][];
        m_amount_Of_Objects_To_Instantiate = new int[m_objects_To_Instantiate.Length];
    }
}

