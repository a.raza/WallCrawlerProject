﻿using UnityEngine;

public class Controller : MonoBehaviour 
{
    private float m_xaxis, m_zaxis;
    private Vector3 m_postion;
    private Transform m_cam;

    void Awake()
    {
        m_cam = FindObjectOfType<Camera>().transform;
    }

    void Update () 
    {
        m_postion = Position ();
    }

    private Vector3 Position()
    {
        m_xaxis = Input.GetAxis ("Horizontal");
        m_zaxis = Input.GetAxis ("Vertical");

        // if player is using the controls...
        if (Is_controlInUse().Equals(true)) 
        {
            var a_forward = m_cam.TransformDirection(Vector3.forward);

            // Player is moving on ground, Y component of camera facing is not relevant.
            a_forward.y = 0.0f;
            a_forward = a_forward.normalized;

            // Calculate target direction based on camera forward and direction key.
            var a_right = new Vector3( a_forward.z, 0, -a_forward.x);
            var a_targetDirection = a_forward * m_zaxis + a_right * m_xaxis;

            return a_targetDirection;
        } 
        else 
        {
            return Vector3.zero;
        }
    }

    public Vector3 GetPosition()
    {
        return m_postion;
    }

    protected bool Is_controlInUse()
    {
        return (!Input.GetAxis("Horizontal").Equals(0)
                ||
                !Input.GetAxis("Vertical").Equals(0)
                ||
                !Input.GetAxis("Horizontal").Equals(0)
                &&
                !Input.GetAxis("Vertical").Equals(0));
    }
}